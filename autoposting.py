while True:
    #if True:
    try:
        print("Вошел в цикл")
        import vk_api
        import sqlite3
        import time
        import requests
        import random
        print("Импорт завершен")

        token_group = ""            #Сюда вводить ключ доступа к сообществу по API, необходим доступ к сообщениям сообщества
        id_group = ""        #Сюда вводить id сообщества (с минусом)

        vk_session = vk_api.VkApi(token=token_group)
        vk = vk_session.get_api()

        conn = sqlite3.connect('base.db')           #подключение к базе
        c = conn.cursor()
        count = 0
        resp_conn  = vk.messages.getLongPollServer(need_pts = 1)            #Подключение к лонгполлу ВК
        pts = resp_conn["pts"]
        print("Авторизация успешна")
        while True:
            resp = vk.messages.getLongPollHistory(pts = pts)            #Получение новых пообщений
            if resp["messages"]["count"] != 0:
                for mess in resp["messages"]["items"]:          #Цикл для новых сообщений
                    if mess["out"] == 0:
                        print("{} {} {}".format(mess["id"], mess["user_id"], mess["body"]))
                        text = mess["body"].lower()
                        if text[:43] == "https://api.vk.com/blank.html#access_token=":          #Если человек отправляет ссылку с токеном
                            try:
                                list = text.split("#")[1].split("&")
                                token = list[0].split("=")[1]
                                user_id = list[2].split("=")[1]
                            except:
                                vk.messages.send(user_id=mess["user_id"], message="Ошибка, не верная ссылка.\nПосмотреть инструкцию можно набрав Помощь")
                                continue
                            if int(user_id) != mess["user_id"]:
                                vk.messages.send(user_id=mess["user_id"], message="Можно использовать только свой токен")
                                continue
                            vk.messages.send(user_id = mess["user_id"], message = "Проверка токена")
                            try:
                                token_check = requests.get("https://api.vk.com/method/messages.send?v=5.80&access_token={}&peer_id={}&message=Проверка успешна".format(token, id_group))            #Проверка токена на валидность
                                result = token_check.json()["response"]
                            except:
                                vk.messages.send(user_id=mess["user_id"], message="Ошибка, токен в ссылке не верный!\nПосмотреть инструкцию можно набрав Помощь")
                                continue
                            c.execute("select exists(select * from users where id = ?)", (user_id,))            #Запись или обновление токена в базе
                            if c.fetchall()[0][0] == 0:
                                c.execute("insert into users values (?, ?, 0)", (user_id, token,))
                                conn.commit()
                                vk.messages.send(user_id=mess["user_id"], message="Токен успешно сохранен\nЧтобы узнать, как создать задание, напишите Создать задание")
                            else:
                                c.execute("update users set token = ? where id = ?", (token, user_id,))
                                conn.commit()
                                vk.messages.send(user_id=mess["user_id"], message="Токен обновлен\nЧтобы узнать, как создать задание, напишите Создать задание")
                            c.execute("update users set fails = 0 where id = ?", (user_id,))
                            conn.commit()
                        elif text == "создать задание":
                            vk.messages.send(user_id=mess["user_id"], message="vk.com/@ilyanik0103-createjob")
                        elif text == "связать задание":
                            vk.messages.send(user_id=mess["user_id"], message="vk.com/@ilyanik0103-linkjob")
                        elif text[:16] == "связать задание " or text[:16] == "связать задания ":            #Если пользователь связывает задание
                            c.execute("select exists(select * from users where id = ?)", (mess["user_id"],))
                            if c.fetchall()[0][0] == 1:         #Проверка пользователя в базе
                                if True:
                                #try:
                                    split_mess = text.split(" ")
                                    message = " ".join(mess["body"].split(" ")[5:])
                                    chat_id = split_mess[3]
                                    delay = split_mess[4]           #Парсинг сообщения на id чата, задержку и id задания для связки
                                    job_id = split_mess[2]
                                    if chat_id.isdigit() == True or chat_id[1:].isdigit() == True:          #Проверка на валидность id чата и исправление номера беседы
                                        if chat_id.isdigit() == True and int(chat_id) < 10000:
                                            chat_id = int(chat_id) + 2000000000
                                    else:
                                        vk.messages.send(user_id=mess["user_id"],
                                                         message="Неправильный запрос на создание задания\n"
                                                                 "Подробнее о создании задания написано в интрукции: vk.com/@ilyanik0103-createjob")
                                        continue
                                    c.execute("select exists(select * from jobs where id = ?)", (job_id,))          #Проверка id задания для связки
                                    if c.fetchall()[0][0] == 1:
                                         pass
                                    else:
                                        vk.messages.send(user_id=mess["user_id"], message="Задания с id {} не существует. id задания можно посмотреть набрав Задания\nУчтите что id задания отличается от номера задания и начинается с d".format(job_id))
                                        continue
                                    try:
                                        if delay[-1] == "с":
                                            s_delay = int(delay[:-1])
                                        elif delay[-1] == "м":
                                            s_delay = int(delay[:-1])*60
                                        elif delay[-1] == "ч":
                                            s_delay = int(delay[:-1])*3600          #преобразование времени в секунды
                                        elif delay[-1] == "д":
                                            s_delay = int(delay[:-1])*86400
                                        elif delay.isdigit() == True:
                                            s_delay = int(delay)
                                        else:
                                            vk.messages.send(user_id=mess["user_id"],
                                                             message="Неправильный запрос на создание задания\n"
                                                                     "Подробнее о создании задания написано в интрукции: vk.com/@ilyanik0103-createjob")
                                            continue
                                    except:
                                        vk.messages.send(user_id=mess["user_id"],
                                                         message="Неправильный запрос на создание задания\n"
                                                                 "Подробнее о создании задания написано в интрукции: vk.com/@ilyanik0103-createjob")
                                        continue
                                    c.execute("select max(nom) from jobs where user_id = ?", (mess["user_id"],))            #Вызов максимального значение номера задания
                                    max_nom = c.fetchall()[0][0]
                                    if max_nom == None:
                                        max_nom = 0
                                    c.execute("select id from jobs order by  _rowid_ desc limit 1")             #Вызов максимального значениея id задания
                                    max_id = c.fetchall()[0][0]
                                    c.execute("select * from jobs where id = ?", (job_id,))         #Выбор связываемого задания
                                    job_main = c.fetchall()[0]
                                    job_time = job_main[3]
                                    if job_main[6] < 1800:
                                        vk.messages.send(user_id=mess["user_id"], message="Связывать задания можно только с заданиями, у которых частота отправки более 30 минут, во избежание капчи")
                                        continue
                                    c.execute("insert into jobs values (?, ?, ?, ?, ?, ?, ?, 0, ?, ?)", (mess["user_id"], chat_id, max_nom + 1, job_time + s_delay, s_delay, message, 0, "d" + str(int(max_id[1:]) + 1), job_id,))
                                    conn.commit()           #Запись задания в базу
                                    vk.messages.send(user_id=mess["user_id"], message="Задание успешно связано\nСписок заданий можно увидеть, набрав Задания")

                            else:
                                vk.messages.send(user_id=mess["user_id"], message="Вы еще не авторизовались в боте. \nЧтобы получить инструкцию по авторизации, напишите: Токен")

                        elif text[:16] == "создать задание ":           #Если пользователь создает задание
                            c.execute("select exists(select * from users where id = ?)", (mess["user_id"],))            #проверка пользователя в базе
                            if c.fetchall()[0][0] == 1:
                                #if True:
                                try:
                                    split_mess = text.split(" ")
                                    message = " ".join(mess["body"].split(" ")[4:])
                                    chat_id = split_mess[2]
                                    delay = split_mess[3]           #Парсинг задания
                                    delay_split = delay.split("-")
                                    try:
                                        delay2 = delay_split[1]
                                        delay = delay_split[0]
                                    except:
                                        delay2 = 0
                                    if chat_id.isdigit() == True or chat_id[1:].isdigit() == True:
                                        if chat_id.isdigit() == True and int(chat_id) < 10000:          #Проверка id чата и исправление номера кф
                                            chat_id = int(chat_id) + 2000000000
                                    else:
                                        vk.messages.send(user_id=mess["user_id"],
                                                         message="Неправильный запрос на создание задания\n"
                                                                 "Подробнее о создании задания написано в интрукции: vk.com/@ilyanik0103-createjob")
                                        continue
                                    try:
                                        if delay[-1] == "с":
                                            s_delay = int(delay[:-1])
                                        elif delay[-1] == "м":
                                            s_delay = int(delay[:-1])*60
                                        elif delay[-1] == "ч":
                                            s_delay = int(delay[:-1])*3600              #преобразование времени в секунды
                                        elif delay[-1] == "д":
                                            s_delay = int(delay[:-1])*86400
                                        elif delay.isdigit() == True:
                                            s_delay = int(delay)
                                        else:
                                            vk.messages.send(user_id=mess["user_id"],
                                                             message="Неправильный запрос на создание задания\n"
                                                                     "Подробнее о создании задания написано в интрукции: vk.com/@ilyanik0103-createjob")
                                            continue
                                        if delay2 != 0:
                                            if delay2[-1] == "с":
                                                s_delay2 = int(delay2[:-1])
                                            elif delay2[-1] == "м":
                                                s_delay2 = int(delay2[:-1])*60
                                            elif delay2[-1] == "ч":
                                                s_delay2 = int(delay2[:-1])*3600            #преобразование второй задержки в секунды
                                            elif delay2[-1] == "д":
                                                s_delay2 = int(delay2[:-1])*86400
                                            elif delay2.isdigit() == True:
                                                s_delay2 = int(delay2)
                                            else:
                                                vk.messages.send(user_id=mess["user_id"],
                                                                 message="Неправильный запрос на создание задания\n"
                                                                         "Подробнее о создании задания написано в интрукции: vk.com/@ilyanik0103-createjob")
                                                continue
                                            sdelay = (s_delay + s_delay2)/2         #Рассчет вреднего времени вмежду задержками
                                            if s_delay2 < 30:
                                                vk.messages.send(user_id=mess["user_id"],
                                                                 message="Частота отправки не может быть менее 30с")
                                                continue
                                        else:
                                            sdelay = s_delay
                                            s_delay2 = 0
                                        if s_delay < 30:
                                            vk.messages.send(user_id=mess["user_id"], message="Частота отправки не может быть менее 30с")
                                            continue
                                    except:
                                        vk.messages.send(user_id=mess["user_id"],
                                                         message="Неправильный запрос на создание задания\n"
                                                                 "Подробнее о создании задания написано в интрукции: vk.com/@ilyanik0103-createjob")
                                        continue
                                    c.execute("select max(nom) from jobs where user_id = ?", (mess["user_id"],))        #Выбор максимального значения номера задания пользователя
                                    max_nom = c.fetchall()[0][0]

                                    if max_nom == None:
                                        max_nom = 0

                                    if sdelay <= 60:
                                        c.execute("select count(*) from jobs where user_id = ? and sdelay <= 60", (mess["user_id"],))           #Ограничение количества заданий пользователя по времени
                                        delay_count = c.fetchall()[0][0]
                                        if delay_count != None and delay_count > 0:
                                            vk.messages.send(user_id=mess["user_id"], message="Заданий с частотой отправки 60 сек и менее не может быть более одного во избежание капчи")
                                            continue

                                    if sdelay <= 120:
                                        c.execute("select count(*) from jobs where user_id = ? and sdelay <= 120", (mess["user_id"],))
                                        delay_count = c.fetchall()[0][0]
                                        if delay_count != None and delay_count > 1:
                                            vk.messages.send(user_id=mess["user_id"], message="Заданий с частотой отправки 2 минуты и менее не может быть более двух во избежание капчи")
                                            continue

                                    if sdelay <= 300:
                                        c.execute("select count(*) from jobs where user_id = ? and sdelay <= 300", (mess["user_id"],))
                                        delay_count = c.fetchall()[0][0]
                                        if delay_count != None and delay_count > 4:
                                            vk.messages.send(user_id=mess["user_id"], message="Заданий с частотой отправки 5 минут и менее не может быть более пяти во избежание капчи")
                                            continue

                                    if sdelay <= 600:
                                        c.execute("select count(*) from jobs where user_id = ? and sdelay <= 600", (mess["user_id"],))
                                        delay_count = c.fetchall()[0][0]
                                        if delay_count != None and delay_count > 9:
                                            vk.messages.send(user_id=mess["user_id"], message="Заданий с частотой отправки 10 минут и менее не может быть более десяти во избежание капчи")
                                            continue

                                    if sdelay <= 1800:
                                        c.execute("select count(*) from jobs where user_id = ? and sdelay <= 1800", (mess["user_id"],))
                                        delay_count = c.fetchall()[0][0]
                                        if delay_count != None and delay_count > 29:
                                            vk.messages.send(user_id=mess["user_id"], message="Заданий с частотой отправки 30 минут и менее не может быть более 30-ти во избежание капчи")
                                            continue

                                    if sdelay <= 3600:
                                        c.execute("select count(*) from jobs where user_id = ? and sdelay <= 3600", (mess["user_id"],))
                                        delay_count = c.fetchall()[0][0]
                                        if delay_count != None and delay_count > 59:
                                            vk.messages.send(user_id=mess["user_id"], message="Заданий с частотой отправки 60 минут и менее не может быть более 60-ти во избежание капчи")
                                            continue

                                    c.execute("select token from users where id = ?", (mess["user_id"],))       #Запрос токена пользователя
                                    token = c.fetchall()[0][0]
                                    message_send = requests.get("https://api.vk.com/method/messages.send?v=5.80&access_token={}&peer_id={}&message={}".format(token, chat_id, message,)).json()     #Отправка первого сообщения и проверка его на валидность
                                    try:
                                        if message_send["error"]["error_code"] == 5:        #Отсеивание ошибок
                                            c.execute("select value from text where key = 'token_link'")
                                            vk.messages.send(user_id=mess["user_id"],message="Ошибка при создании задания, не действительный токен\nОткройте ссылку {} и пришлите сюда новый токен, как это делали ранее\nЧтобы получить инструкцию по обновлению токена, напишите Токен".format(c.fetchall()[0][0]))
                                            continue
                                        elif message_send["error"]["error_code"] == 7 or message_send["error"]["error_code"] == 10:
                                            vk.messages.send(user_id=mess["user_id"],message="Ошибка при создании задания, чат удален или еще не создан")
                                            continue
                                        elif message_send["error"]["error_code"] == 14:
                                            vk.messages.send(user_id=mess["user_id"],message="Ошибка при создании задания, необходим ввод капчи. Уменьшите частоту отправки сообщений до ~1 сообщения в 30 сек")
                                            continue
                                        elif message_send["error"]["error_code"] == 902:
                                            vk.messages.send(user_id=mess["user_id"],message="Ошибка при создании задания, вы добавлены в черный список этого чата")
                                            continue
                                        else:
                                            vk.messages.send(user_id=mess["user_id"],message="Ошибка при создании задания.\nВозможно вы потеряли доступ к этому чату или вас добавли в ЧС, ну или вы неправильно указали данные в задании")
                                            continue
                                    except:
                                        pass
                                    c.execute("select id from jobs order by  _rowid_ desc limit 1")     #Выбор максимального id задания
                                    try:
                                        max_id = c.fetchall()[0][0]
                                    except:
                                        max_id = "d0"       #Если заданий в базе нет, то id становится d0, но в базу записывается d1
                                    if s_delay2 != 0:
                                        if s_delay2 < s_delay:
                                            ns_delay = s_delay      #Замена, чтобы наименьшее значение задержки было впереди ля работы random.randint()
                                            s_delay = s_delay2
                                            s_delay2 = ns_delay
                                        c.execute("insert into jobs values (?, ?, ?, ?, ?, ?, ?, 0, ?, 0)",(mess["user_id"], chat_id, max_nom + 1, time.time() + random.randint(s_delay, s_delay2), "{}-{}".format(s_delay, s_delay2), message, (s_delay + s_delay2)/2, "d" + str(int(max_id[1:]) + 1),))           #Запись в базу задания, если есть диапазон задержек
                                    else:
                                        c.execute("insert into jobs values (?, ?, ?, ?, ?, ?, ?, 0, ?, 0)", (mess["user_id"], chat_id, max_nom + 1, time.time() + s_delay, s_delay, message, s_delay, "d" + str(int(max_id[1:]) + 1),))         #Запись задания в базу,если диапазона задержек нет
                                    conn.commit()
                                    vk.messages.send(user_id=mess["user_id"], message="Задание успешно создано\nСписок заданий можно увидеть, набрав Задания")
                                #if False:
                                except:
                                    vk.messages.send(user_id=mess["user_id"], message="Неправильный запрос на создание задания\n"
                                                                                      "Подробнее о создании задания написано в интрукции: vk.com/@ilyanik0103-createjob")
                                    continue
                            else:
                                vk.messages.send(user_id=mess["user_id"], message="Вы еще не авторизовались в боте. \nЧтобы получить инструкцию по авторизации, напишите: Токен")
                        elif text == "задания":         #Если пользователь запрашивает задания
                            c.execute("select * from jobs where user_id = ?", (mess["user_id"],))       #Выбор заданий пользователя
                            jobs = c.fetchall()
                            if jobs != []:
                                job_list = "Список активных заданий:\nномер задания | id задания | id чата | чатота | сообщение\n"
                                for job in jobs:
                                    if job[9] != 0:
                                        job_connect = "| Связано с заданием {}".format(job[9])
                                    else:
                                        job_connect = ""
                                    job_list = job_list + "{} | {} | {} | {}с | {} {}\n".format(job[2], job[8], job[1], job[4], job[5], job_connect)
                                job_list = job_list + "\nЧтобы удалить задание напишите: Удалить (номер задания)\nЧтобы создать задание напишите: Создать задание"
                                vk.messages.send(user_id=mess["user_id"], message=job_list)
                            else:
                                vk.messages.send(user_id=mess["user_id"],
                                                 message="Вы пока не создали ни одного задания\nПодробнее о создании задания написано в интрукции: vk.com/@ilyanik0103-createjob")
                                continue
                        elif text == "удалить все" or text == "удалить всё":
                            c.execute("delete from jobs where user_id = ?", (mess["user_id"],))
                            conn.commit()
                            vk.messages.send(user_id=mess["user_id"], message="Все задания успешно удалены")
                        elif text == "удалить":
                            vk.messages.send(user_id=mess["user_id"], message="Чтобы удалить задание, напишите: Удалить (номер задания)\nНомер задания можно посмотреть в списке заданий (команда - Задания)")
                        elif text[:8] == "удалить ":
                            nom = text.split(" ")[1]
                            c.execute("select exists(select * from jobs where nom = ? and user_id = ?)", (nom, mess["user_id"],))
                            if c.fetchall()[0][0] != 0:
                                c.execute("delete from jobs where user_id = ? and nom = ?", (mess["user_id"], nom,))
                                conn.commit()
                                vk.messages.send(user_id=mess["user_id"], message="Задание {} удалено".format(nom))
                            else:
                                vk.messages.send(user_id=mess["user_id"], message="Задания с таким номером не существует\nЧтобы посмотреть все задания наберите: Задания")
                        elif text == "токен" or text == "логин":
                            vk.messages.send(user_id=mess["user_id"], message="vk.com/@ilyanik0103-botauth")
                        elif text == "помощь" or text == "старт" or text == "помощ" or text == "хелп" or text == "start" or text == "help" or text == "команды" or text == "комманды" or text == "список команд" or text == "список комманд":
                            vk.messages.send(user_id=mess["user_id"], message="Список команд бота:\n"
                                                                              "💬Токен - вызов инструкции по авторизации в боте\n"
                                                                              "💬FAQ - вызов FAQ :)\n"
                                                                              "💬Задания - список ваших заданий\n"
                                                                              "💬Создать задание (id чата) (частота) (сообщение) - добавление задания\n"
                                                                              "💬Создать задание (id чата) (частота-частота) (сообщение) - добавление задания со случайным временем отправки в заданном диапазоне\n"
                                                                              "💬Создать задание - вызов инструкции по добавлению задания\n"
                                                                              "💬Связать задание (id связываемого задания) (id чата) (задержка от связываемого задания) (сообщение) - добавление задания с привязкой к другому заданию\n"
                                                                              "💬Связать задание - вызов инструкции по привязке задания\n"
                                                                              "💬Удалить (номер задания) - удалить определенное задание\n"
                                                                              "💬Удалить - вызов инструкции по удалению задания\n")
                        elif text == "faq":
                            vk.messages.send(user_id=mess["user_id"], message="https://vk.com/@ilyanik0103-faq")
                        else:
                            if text != "проверка успешна":
                                vk.messages.send(user_id=mess["user_id"], message="Неверная команда. Чтобы посмотреть список команд, наберите Команды\n"
                                                                                  "Чтобы посмотреть инструкцию по работе с заданиями, наберите Помощь")
            if True:        #Отправка заданий. Тут можно ставить условие, при котором будет проверяться наличие заданий, готовых к отправке. По умолчанию проверка идет при каждомзапросе к лонгполл, тобиш примерно 3 раза в секунду
                time_now = time.time()
                c.execute("select exists (select * from jobs where time < ?)", (time_now,))     #Проверка наличия заданий, готовых к отправке
                if c.fetchall()[0][0] == 1:
                    c.execute("select * from jobs where time < ?", (time_now,))         #Запрос у базы заданий, готовых к отправке
                    jobs_end = c.fetchall()
                    for job in jobs_end:        #Цикл заданий
                        c.execute("select * from users where id = ?", (job[0],))        #Запрос информации о пользователе
                        user_info = c.fetchall()[0]
                        token = user_info[1]
                        if user_info[2] == 5:           #Если у пользователя 5 неудачных отправок из-за битого токена
                            vk.messages.send(user_id=job[0], message="Совершено 5 неудачных попыток отправки сообщения по опричине неверного токена, обновите токен по инструкции выше")
                            c.execute("update users set fails = fails + 1 where id = ?", (job[0],))         #Запрос информации о задании
                            conn.commit()
                            continue
                        if job[7] == 10:            #Если совершено 10 неудачных попыток отправки задания + удаление этого задания
                            vk.messages.send(user_id=job[0], message="Совершено 10 неудачных попыток отправки сообщения по опричине недоступности чата\nЗадание удалено, проверьте id чата и попробуйте создать задание заново")
                            c.execute("delete from jobs where nom = ? and user_id = ?", (job[2], job[0],))
                            conn.commit()
                            continue
                        if user_info[2] > 5:
                            continue
                        #if True:
                        try:
                            message_send = requests.get("https://api.vk.com/method/messages.send?v=5.80&access_token={}&peer_id={}&message={}".format(token, job[1], job[5],)).json()           #Отправка сообщения из задания
                            job_split = job[4].split("-")
                            #if True:
                            if job[9] != 0:
                                c.execute("select time from jobs where id = ?", (job[9],))      #Запрос времени основного задания, если оно связанное
                                try:
                                    conn_time = c.fetchall()[0][0]
                                    if conn_time != []:
                                        c.execute("update jobs set time = ? where user_id = ? and nom = ?", (conn_time + int(job[4]), job[0], job[2],))     #Обновление задания
                                except:
                                    c.execute("delete from jobs where user_id = ? and nom = ?", (job[0], job[2],))      #Удаление связанного задания, если родительское задание удалено
                                    vk.messages.send(user_id=job[0], message="Ошибка связанного задания номер {}. Сообщение не отправлено, так как родительское задание удалено\nВсе связанные с ним задания будут удалены".format(job[2]))
                                conn.commit()
                                continue
                            try:
                                new_time2 = int(job_split[1])
                                new_time = int(job_split[0])
                                c.execute("update jobs set time = ? where user_id = ? and nom = ?",(round(time_now + random.randint(new_time, new_time2)), job[0], job[2],))        #Обновление времени задания, если в нем есть диапазон времени
                            except:
                                c.execute("update jobs set time = ? where user_id = ? and nom = ?", (round(time_now + int(job[4])), job[0], job[2],))           #Обновление времени задания, если в нем фиксированная задержка
                            conn.commit()
                        #if False:
                        except:
                            continue
                        try:            #Работа с ошибками, если они есть
                            if message_send["error"]["error_code"] == 5:
                                c.execute("select value from text where key = 'token_link'")
                                vk.messages.send(user_id=job[0], message="Ошибка при отправке сообщения, не действительный токен\nОткройте ссылку {} и пришлите сюда новый токен, как это делали ранее\nЧтобы получить инструкцию по обновлению токена, напишите Токен\nЕсли эта ошибка не повторяется, никакие действия не требуются".format(c.fetchall()[0][0]))
                                c.execute("update users set fails = fails + 1 where id = ?", (job[0],))
                                conn.commit()
                            elif message_send["error"]["error_code"] == 7 or message_send["error"]["error_code"] == 10:
                                vk.messages.send(user_id=job[0], message="Ошибка при отправке сообщения по заданию {}, чат удален или еще не создан".format(job[2]))
                                c.execute("update jobs set fails = fails + 1 where nom = ? and user_id = ?", (job[2], job[0],))
                                conn.commit()
                            elif message_send["error"]["error_code"] == 902:
                                c.execute("update jobs set fails = fails + 1 where nom = ? and user_id = ?", (job[2], job[0],))
                                conn.commit()
                                vk.messages.send(user_id=job[0], message="Ошибка при отправке сообщения по заданию {}, вы добавлены в черный список этого чата".format(job[2]))
                            elif message_send["error"]["error_code"] == 14:
                                vk.messages.send(user_id=job[0], message="Ошибка при отправке сообщения по заданию {}, необходим ввод капчи. Уменьшите частоту отправки сообщений до ~1 сообщения в 30 сек".format(job[2]))
                            else:
                                vk.messages.send(user_id=job[0], message="Ошибка при отправке сообщения по заданию {}.\nВозможно вы потеряли доступ к этому чату или вас добавли в ЧС, ну или вы неправильно указали данные в задании\nЕсли эта ошибка не повторяется, действия не требуются".format(job[2]))
                        except:
                            c.execute("update users set fails = 0 where id = ?", (job[0],))
                            c.execute("update jobs set fails = 0 where nom = ? and user_id = ?", (job[2], job[0],))         #Сброс счетчика ошибок задания и токена пользователя
                            conn.commit()


            pts = resp["new_pts"]           #обозначение нового pts
    #if False:
    except BaseException:           #Перезапуск бота в случае ошибок
        print("Перезапуск")
        time.sleep(1)
